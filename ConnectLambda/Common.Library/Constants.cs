﻿using System;

namespace Common.Library
{
    public class Constants
    {
        public static readonly string TABLE_NAME = "MRM_Connections";

        public const string ConnectionIdField = "ConnectionId";

        public const string Enterprise = "Enterprise";

        public const string Division = "Division";

        public const string BU = "BU";

        public const string CreatedDate = "CreatedDate";
    }
}
